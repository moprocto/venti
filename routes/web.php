<?php

use App\Http\Controllers\Auth\EmailVerificationController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Livewire\Auth\Login;
use App\Http\Livewire\Auth\Passwords\Confirm;
use App\Http\Livewire\Auth\Passwords\Email;
use App\Http\Livewire\Auth\Passwords\Reset;
use App\Http\Livewire\Auth\Register;
use App\Http\Livewire\Auth\Verify;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('/phrases', [SearchController::class, 'phrases'])->name('phrases');

Route::get('/search', [SearchController::class, 'index'])->name('search');
Route::get('/signin', function(){
    Auth::logout();
    return redirect('/login');
});
Route::get('/populate', [EventController::class, 'populate'])->name('populate');
Route::get('/generate', [EventController::class, 'generate'])->name('generate');
Route::post('/registration', [RegisterController::class, 'add'])->name('registration');
Route::post('/confirmSMS', [RegisterController::class, 'confirmSMS'])->name('confirm-sms');

Route::middleware('guest')->group(function () {
    Route::get('login', Login::class)->name('login');
    Route::get('register', Register::class)->name('register');
});

Route::get('password/reset', Email::class)->name('password.request');

Route::get('password/reset/{token}', Reset::class)->name('password.reset');

Route::middleware('auth')->group(function () {
    Route::get('email/verify', Verify::class)
        ->middleware('throttle:6,1')
        ->name('verification.notice');

    Route::get('/verify', [RegisterController::class, 'getVerify'])->name('verify');

    Route::get('password/confirm', Confirm::class)->name('password.confirm');
});

Route::middleware('auth')->group(function () {
    Route::get('email/verify/{id}/{hash}', EmailVerificationController::class)
        ->middleware('signed')
        ->name('verification.verify');

    Route::post('logout', LogoutController::class)->name('logout');
});

Route::get('event/{id}/{source}', [EventController::class, 'read']);

Route::view('chat', 'chat')->middleware('auth');
Route::get('/private', [EventController::class, 'privateRooms'])->name('private');

Route::get('/hooks/block/channel/{channel}', [EventController::class, 'blockChannel']);
Route::get('/hooks/delete/video/{video}', [EventController::class, 'delete']);

Route::get('/random', [EventController::class, 'random']);



Route::get('/{category?}', [EventController::class, 'browse'])->name('home');



//Route::view('home', 'home')
        //->middleware('auth')->name('home');
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/nursing', [HomeController::class, 'nursing']);
Route::get('/lawyers', [HomeController::class, 'lawyers']);
Route::post('/signup', [HomeController::class, 'register']);
Route::get('/m', [HomeController::class, 'lists']);


