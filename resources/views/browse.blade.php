@extends('layouts.master')

@section('css')
		div.content{
            border-top-left-radius: 30px !important;
        }
        html, body{
        background-color:#222b3b !important;
    }
    @media only screen and (max-width: 600px) {
	    .blog__preview div.absolute a.block{
		    text-overflow: ellipsis;
		    width: 80vw;
		    overflow: hidden;
		}
	}
@endsection

@section('content')
    @foreach($videos as $video)
    	@include('layouts.video-result', $video)
    @endforeach
@endsection

@section('js')
    <script>
        jQuery(document).ready(function ($) {
            $("body").on("click", "#block", function(){
                var channel = $(this).attr("data-channel");

                $.get( "/hooks/block/channel/" + channel, function( data ) {
                  
                });

                $(this).parent().parent().remove();
            });
        });
    </script>
@endsection
