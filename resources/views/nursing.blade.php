<!DOCTYPE html>
<html lang="zxx">
  <head>
    <meta charset="utf-8" />
    <meta name="author" content="Themezhub" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>venti | Win Up to $2,500 for Passing Your Exam</title>
     <meta property="og:image" content="https://venti.co/images/venti-nursing.png" />
        <!-- Custom CSS -->
        <link href="/assets/css/style.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">
        <style type="text/css">
          .hide{
            display: none !important;
          }
          .slider-horizontal{
            width: 100% !important;
          }
          .hero_banner{
            background-image: url(/images/shapes-background.png);
            background-repeat: repeat !important;
            background-size: auto !important;
            background-position: fixed;
          }
        </style>
    </head>
    <body>

        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
    
            <!-- ============================================================== -->
            <!-- Top header  -->
            <!-- ============================================================== -->
            <!-- Start Navigation -->
      <div class="header header-transparent dark-text hide">
        <div class="container">
          <nav id="navigation" class="navigation navigation-landscape">
            <div class="nav-header">
              <a class="nav-brand" href="#">
                <img src="https://themezhub.net/skillup-demo/assets/img/logo.png" class="logo" alt="" />
              </a>
              <div class="nav-toggle"></div>
              <div class="mobile_nav">
                <ul>
                  <li>
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#login" class="crs_yuo12 w-auto text-white theme-bg">
                      <span class="embos_45"><i class="fas fa-sign-in-alt mr-1"></i>Sign In</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="nav-menus-wrapper">
              <ul class="nav-menu">
              
                <li class="active"><a href="#">Home<span class="submenu-indicator"></span></a>
                  <ul class="nav-dropdown nav-submenu">
                    <li><a href="index.html">Home 1</a></li>
                    <li><a href="home-2.html">Home 2</a></li>
                    <li><a href="home-3.html">Home 3</a></li>
                    <li><a href="home-4.html">Home 4</a></li>
                    <li><a href="home-5.html">Home 5</a></li>
                    <li><a href="home-6.html">Home 6</a></li>
                    <li><a href="home-7.html">Home 7</a></li>
                  </ul>
                </li>
                
                <li><a href="#">Courses<span class="submenu-indicator"></span></a>
                  <ul class="nav-dropdown nav-submenu">
                    <li><a href="#">Search Courses in Grid<span class="submenu-indicator"></span></a>
                      <ul class="nav-dropdown nav-submenu">
                        <li><a href="grid-layout-with-sidebar.html">Grid Layout Style 1</a></li>
                        <li><a href="grid-layout-with-sidebar-2.html">Grid Layout Style 2</a></li>
                        <li><a href="grid-layout-with-sidebar-3.html">Grid Layout Style 3</a></li>
                        <li><a href="grid-layout-with-sidebar-4.html">Grid Layout Style 4</a></li>
                        <li><a href="grid-layout-with-sidebar-5.html">Grid Layout Style 5</a></li>
                        <li><a href="grid-layout-full.html">Grid Full Width</a></li>
                        <li><a href="grid-layout-full-2.html">Grid Full Width 2</a></li>
                        <li><a href="grid-layout-full-3.html">Grid Full Width 3</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Search Courses in List<span class="submenu-indicator"></span></a>
                      <ul class="nav-dropdown nav-submenu">
                        <li><a href="list-layout-with-sidebar.html">List Layout Style 1</a></li>
                        <li><a href="list-layout-with-full.html">List Full Width</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Courses Detail<span class="submenu-indicator"></span></a>
                      <ul class="nav-dropdown nav-submenu">
                        <li><a href="course-detail.html">Course Detail 01</a></li>
                        <li><a href="course-detail-2.html">Course Detail 02</a></li>
                        <li><a href="course-detail-3.html">Course Detail 03</a></li>
                        <li><a href="course-detail-4.html">Course Detail 04</a></li>
                      </ul>
                    </li>
                    
                    <li><a href="explore-category.html">Explore Category</a></li>
                    <li><a href="find-instructor.html">Find Instructor</a></li>
                    <li><a href="instructor-detail.html">Instructor Detail</a></li>
                  </ul>
                </li>
                
                <li><a href="#">Pages<span class="submenu-indicator"></span></a>
                  <ul class="nav-dropdown nav-submenu">
                    <li><a href="#">Shop Pages<span class="submenu-indicator"></span></a>
                      <ul class="nav-dropdown nav-submenu">
                        <li><a href="shop-with-sidebar.html">Shop Sidebar Left</a></li>
                        <li><a href="shop-with-right-sidebar.html">Shop Sidebar Right</a></li>
                        <li><a href="list-shop-with-sidebar.html">Shop List Style</a></li>
                        <li><a href="wishlist.html">Wishlist</a></li>
                        <li><a href="checkout.html">Checkout</a></li>
                        <li><a href="product-detail.html">Product Detail</a></li>
                      </ul>
                    </li>
                    <li><a href="about.html">About Us</a></li>
                    <li><a href="contact.html">Say Hello</a></li>
                    <li><a href="blogs.html">Blog Style</a></li>
                    <li><a href="pricing.html">Pricing</a></li>
                    <li><a href="404.html">404 Page</a></li>
                    <li><a href="component.html">Elements</a></li>
                    <li><a href="faq.html">FAQs</a></li>
                    <li><a href="login.html">Login</a></li>
                    <li><a href="signup.html">Signup</a></li>
                    <li><a href="forgot.html">Forgot</a></li>
                  </ul>
                </li>
                
                <li><a href="dashboard.html">Dashboard</a></li>
                
              </ul>
              
              <ul class="nav-menu nav-menu-social align-to-right">
                
                <li>
                  <a href="#" class="alio_green" data-toggle="modal" data-target="#login">
                    <i class="fas fa-sign-in-alt mr-1"></i><span class="dn-lg">Sign In</span>
                  </a>
                </li>
                <li class="add-listing theme-bg">
                  <a href="signup.html" class="text-white">Get Started</a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
      <!-- End Navigation -->
      <div class="clearfix"></div>
      <!-- ============================================================== -->
      <!-- Top header  -->
      <!-- ============================================================== -->

      <!-- ============================ Hero Banner  Start================================== -->
      <div class="hero_banner image-cover image_bottom h4_bg" style="">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="simple-search-wrap text-left">
                <div class="hero_search-2">
                  <p style="font-size:28px;">venti</p>
                  <h1 class="banner_title mb-4">Win up to $2,500</h1>
                  <h2 class="mb-4">For passing the <span id="exam-rotator">RN</span> certification exam</h2>


                <div class="text-left"><a href="https://apptestingco.typeform.com/to/lxXxBGfa" target="_blank" class="btn btn-md text-light theme-bg">Join Wait List</a></div>
                <br>
                <a href="#calculate" class="btn btn-outline-theme">See how it works!</a>

                  <div class="crs_trio_info hide">
                    <div class="crs_trio">
                      <h4 class="ctr"><span class="rt_count">72</span>K</h4>
                      <span class="pol_title">Appreciations</span>
                    </div>
                    <div class="crs_trio">
                      <h4 class="ctr"><span class="rt_count">100</span></h4>
                      <span class="pol_title">Countries</span>
                    </div>
                    <div class="crs_trio">
                      <h4 class="ctr"><span class="rt_count">5.2</span>K +</h4>
                      <span class="pol_title">Cources</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="side_block">
                <img src="/images/nurses.png" class="img-fluid" alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- ============================ Hero Banner End ================================== -->
      

    
      
      <!-- ============================ Work Process Start ================================== -->
      <section class="imageblock" id="calculate">
        
        <div class="container">
          
          <div class="row align-items-center justify-content-between">
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
              <div class="sec-heading center">
                <h2>Introducing <span class="theme-cl">Study Pools</span></h2>
                <p class="">We invented a fun way to prepare for your upcoming exam! Here's how it works:</p>
              </div>
              <section class="p-0">
                <div class="container">
                  <div class="row justify-content-center">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                      <div class="crp_box fl_color">
                        <div class="row align-items-center" style="margin:10px 0;">
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="dro_140">

                              <div class="dro_142">
                                <h6>Step 1:</h6>
                                <p>Complete a study survey indicating when you plan to test and your ideal studying style.</p>
                              </div>
                            </div>
                          </div>
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="dro_140">
                              <div class="dro_142">
                                <h6>Step 2:</h6>
                                <p>Select a Study Pool with entry deposits ranging from $0 to $1,000.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row align-items-center" style="margin:10px 0;">
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="dro_140">
                              <div class="dro_142">
                                <h6>Step 3:</h6>
                                <p>Complete practice exam questions until your test date. You can also join study groups within your pool!</p>
                              </div>
                            </div>
                          </div>
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="dro_140">
                              <div class="dro_142">
                                <h6>Step 4:</h6>
                                <p>Split the prize pool with everyone that passed the exam.</p>
                              </div>
                            </div>
                          </div>
                          <div class="lmp_caption col-md-12 mb-4">
                            <div class="text-center">
                              <br>
                              <p>We're launching soon!</p>
                              <button data-tf-popup="lxXxBGfa" class="btn btn-md text-light theme-bg">Join Wait List</button><script src="//embed.typeform.com/next/embed.js"></script>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </section>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12" >
              @include('calculator')
            </div>
          </div>
        </div>
      </section>
      
       @include('benefits')

      <section class="imageblock pt-m-0">
        <div class="imageblock__content left">
          <div class="background-image-holder" style="background: url(/images/nurse-studying.jpg); background-position: left !important;">
              <img alt="image" src="/images/nurse-studying.jpg">
          </div>
        </div>
          @include('bold-print')
      </section>
      <div class="clearfix"></div>

      
      <!-- Log In Modal -->
      <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginmodal" aria-hidden="true">
        <div class="modal-dialog modal-xl login-pop-form" role="document">
          <div class="modal-content overli" id="loginmodal">
            <div class="modal-header">
              <h5 class="modal-title">Login Your Account</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
              </button>
              </div>
            <div class="modal-body">
              <div class="login-form">
                <form>
                
                  <div class="form-group">
                    <label>User Name</label>
                    <div class="input-with-icon">
                      <input type="text" class="form-control" placeholder="User or email">
                      <i class="ti-user"></i>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label>Password</label>
                    <div class="input-with-icon">
                      <input type="password" class="form-control" placeholder="*******">
                      <i class="ti-unlock"></i>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <div class="col-xl-4 col-lg-4 col-4">
                      <input id="admin" class="checkbox-custom" name="admin" type="checkbox">
                      <label for="admin" class="checkbox-custom-label">Admin</label>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-4">
                      <input id="student" class="checkbox-custom" name="student" type="checkbox" checked>
                      <label for="student" class="checkbox-custom-label">Student</label>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-4">
                      <input id="instructor" class="checkbox-custom" name="instructor" type="checkbox">
                      <label for="instructor" class="checkbox-custom-label">Tutors</label>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <button type="submit" class="btn btn-md full-width theme-bg text-white">Login</button>
                  </div>
                  
                  <div class="rcs_log_125 pt-2 pb-3">
                    <span>Or Login with Social Info</span>
                  </div>
                  <div class="rcs_log_126 full">
                    <ul class="social_log_45 row">
                      <li class="col-xl-4 col-lg-4 col-md-4 col-4"><a href="javascript:void(0);" class="sl_btn"><i class="ti-facebook text-info"></i>Facebook</a></li>
                      <li class="col-xl-4 col-lg-4 col-md-4 col-4"><a href="javascript:void(0);" class="sl_btn"><i class="ti-google text-danger"></i>Google</a></li>
                      <li class="col-xl-4 col-lg-4 col-md-4 col-4"><a href="javascript:void(0);" class="sl_btn"><i class="ti-twitter theme-cl"></i>Twitter</a></li>
                    </ul>
                  </div>
                
                </form>
              </div>
            </div>
            <div class="crs_log__footer d-flex justify-content-between mt-0">
              <div class="fhg_45"><p class="musrt">Don't have account? <a href="signup.html" class="theme-cl">SignUp</a></p></div>
              <div class="fhg_45"><p class="musrt"><a href="forgot.html" class="text-danger">Forgot Password?</a></p></div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Modal -->
      
      <a id="back2Top" class="top-scroll" title="Back to top" href="#"><i class="ti-arrow-up"></i></a>
      

    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="https://themezhub.net/skillup-demo/assets/js/jquery.min.js"></script>
    <script src="https://themezhub.net/skillup-demo/assets/js/popper.min.js"></script>
    <script src="https://themezhub.net/skillup-demo/assets/js/bootstrap.min.js"></script>
    <script src="https://themezhub.net/skillup-demo/assets/js/select2.min.js"></script>
    <script src="https://themezhub.net/skillup-demo/assets/js/slick.js"></script>
    <script src="https://themezhub.net/skillup-demo/assets/js/moment.min.js"></script>
    <script src="https://themezhub.net/skillup-demo/assets/js/daterangepicker.js"></script> 
    <script src="https://themezhub.net/skillup-demo/assets/js/summernote.min.js"></script>
    <script src="https://themezhub.net/skillup-demo/assets/js/metisMenu.min.js"></script> 
    <script src="https://themezhub.net/skillup-demo/assets/js/custom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->   
    <script type="text/javascript">
    jQuery(document).ready(function ($) {
      displayClass(0);

      

        $(".calculate").on("change", function(){

          var poolSize = $("#pool-size").val();
          var poolAmount = $("#pool-amount").val();
          var poolRate = $("#pool-rate").val();
          console.log(poolAmount);

          if(poolAmount == 1){
              poolAmount = 10;
          }
          else if(poolAmount == 2){
              poolAmount = 20;
          }
          else if(poolAmount == 3){
              poolAmount = 50;
          }
          else if(poolAmount == 4){
              poolAmount = 100;
          }
          else if(poolAmount == 5){
              poolAmount = 500;
          }
          else if(poolAmount == 6){
              poolAmount = 1000;
          }
          var poolPrize = poolAmount * poolSize;

          $("#pool-prize").text("$" + poolPrize.toLocaleString());
          $("#pool-split").text((poolSize * poolRate).toLocaleString('en-US'));

          poolPayout = calculatePayout(poolAmount, poolSize, poolRate);
          poolPayout = poolPayout.toLocaleString("en-US", {minimumFractionDigits: 2, maximumFractionDigits: 2});

          $("#pool-payout").text(poolPayout);

          $("#pool-profit").text((calculatePayout(poolAmount, poolSize, poolRate) - poolAmount).toLocaleString("en-US", {minimumFractionDigits: 2, maximumFractionDigits: 2}));

        });

      function displayClass(i) {
        var classes = [
          'RN',
          'NP',
          'LPN',
          'CNA'
        ];

        $('#exam-rotator').text(classes[i]).fadeIn(1000, function() {
            $(this).delay(600).fadeOut(1000, function() {
                displayClass((i + 1) % classes.length);
            });
        });

        
      }

      function calculatePayout(poolAmount, poolSize, poolRate){
        console.log("size " + poolSize);
        console.log("amount " + poolAmount);
        console.log("rate " + poolRate);

        var top = poolSize * poolAmount;
        var bottom = poolSize * poolRate;

        console.log("top " + top);
        console.log("bottom " + bottom);

        return top/bottom;
      }
    });
    </script>
    @if(env('APP_ENV') == "production")
    <!-- Hotjar Tracking Code for https://venti.co -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2651983,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
      <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-Z6MV4F6SMV"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-Z6MV4F6SMV');
</script>
    @endif
    
  </body>
</html>