<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="dark">
<head>
    <meta charset="utf-8">
    <link href="/images/logo.png" rel="shortcut icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="Watch all the internet's live streams with everyone no matter where they are!">
    <meta name="keywords" content="live videos with chat">
    <meta name="author" content="venti">
    <title>venti: watch it live with everyone, everywhere</title>
    <meta property="og:description" content="Watch all the internet's live streams with everyone no matter where they are!" />
    <meta property="og:image" content="https://venti.co/images/venti-bannersocial.png" />
    <!-- BEGIN: CSS Assets-->
    <script src="{{ url(mix('js/app.js')) }}" defer></script>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/theme.css" />
    <style>
        div.content{
            border-top-left-radius: 0;
            padding-bottom:0;
        }
        body{
            background-color: #0c0f14 !important;
            overflow-y: none;
        }
        .app{
            padding-bottom: 0 !important;
        }
        .search .search__input{
            width: 50em;
        }
        .search-result{
            left:0;
        }
        .search.menu{
            padding-top: 10px;
        }
        @media only screen and (max-width: 600px) {
            .search-result{
                top:50px !important;
                width: 100%;
                left:1em;
                right:0;
            }
            .search.menu{
                padding-top: 0;
            }
        }
        @yield('css')
    </style>
</head>
    <body class="app">
        <div id="tv-static" style="display:none; position: fixed; top: 0; left:0; width: 100%; height: 100%; z-index:999; background-image: url('/images/tv-static.gif')"></div>
        @include('layouts.top-nav')

        @livewireScripts

        <div class="flex">
            <div class="content">
                <div class="intro-y grid grid-cols-12 gap-6 mt-5">
                    @yield('content')
                </div>
            </div>
        </div>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-Z6MV4F6SMV"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-Z6MV4F6SMV');
        </script>        
        <script src="/js/theme.js"></script>
        <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js' id='jquery-core-js'></script>
        <script src="https://unpkg.com/feather-icons"></script>
        <script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>
        <script>
            jQuery(document).ready(function ($) {
                $("#flip-channel").click(function(){
                    $("#tv-static").show();
                });
            });
        </script>
        @yield('js')

    </body>
</html>