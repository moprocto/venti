<div class="mobile-menu md:hidden">
    <div class="mobile-menu-bar">
        <div class="mr-auto">
        <a href="/" style="display: -webkit-box;">
            <img alt="venti" style="width: 40px; height: 30px;" src="/images/logo.png"> <span class="display:md:hidden menu text-lg truncate mr-5 top-menu font-medium" style="display: inline;">@if($page == "top") Top Streams @else {{ ucfirst($page) }} @endif</span>
        </a>
        </div>
        <div>
            <a href="/random" class="menu text-lg truncate mr-5 top-menu"><div class="menu__title"><img src="/images/remote-icon-2.png" style="width: 30px; opacity: 0.8;" title="Play Random Stream" id="flip-channel"></div></a>
        </div>
        <a href="javascript:;" id="mobile-menu-toggler">
            <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i>
        </a>
    </div>
    <ul class="border-t border-theme-24 py-5 hidden">
        <li class="search-holder">
            @livewire('search')
        </li>
		<li>
			<a href="/top" class="menu text-lg truncate mr-5 top-menu @if($page == 'top') top-menu--active font-medium @endif"><div class="menu__title">Top Streams</div></a>
		</li>
        <li>
            <a href="/music" class="menu text-lg truncate mr-5 top-menu @if($page == 'music') top-menu--active font-medium @endif"><div class="menu__title">Music</div></a>
        </li>
        <li>
        	<a href="/gaming" class="menu text-lg truncate mr-5 top-menu @if($page == 'gaming') top-menu--active font-medium @endif"><div class="menu__title">Gaming</div></a>
        </li>
        <li>
            <a href="/finance" class="menu text-lg truncate mr-5 top-menu @if($page == 'finance') top-menu--active font-medium @endif"><div class="menu__title">Finance</div></a>
        </li>
		<li>
        	<a href="/news" class="menu text-lg truncate mr-5 top-menu @if($page == 'news') top-menu--active font-medium @endif"><div class="menu__title">News</div></a>
        </li>
        
        @auth
            @if(Auth::user()->id == 1)
                <li>
                    <a href="/signin" class="menu text-lg truncate mr-5 top-menu"><div class="menu__title">Log in</div></a>
                </li>       
            @else
                <!--
                <li>
                    <a href="/private" class="menu text-lg truncate mr-5 top-menu @if($page == 'private') top-menu--active font-medium @endif"><div class="menu__title">Private</div></a>
                </li>
            -->
                <li class="pull-right">
                    <a href="/" href="{{ route('logout') }}"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="menu text-lg truncate mr-5 top-menu"><div class="menu__title">Logout</div></a>
                </li>
            @endif

            @else
                <li>
                    <a href="{{ route('login') }}" class="menu text-lg truncate mr-5 top-menu"><div class="menu__title">Log in</div></a>
                </li>
        @endauth
    </ul>
</div>
    
	<div class="top-nav">
		<ul>
			<li>
				<a href="/top" class="text-lg truncate mr-5 top-menu"><img alt="Midone Tailwind HTML Admin Template" class="w-6" src="/images/logo.png"></a>
			</li>
			<li>
				<a href="/top" class="text-lg truncate mr-5 top-menu @if($page == 'top') top-menu--active font-medium @endif">Top Streams</a>
			</li>
            <li>
                <a href="/music" class="text-lg truncate mr-5 top-menu @if($page == 'music') top-menu--active font-medium @endif">Music</a>
            </li>
            <li>
            	<a href="/gaming" class="text-lg truncate mr-5 top-menu @if($page == 'gaming') top-menu--active font-medium @endif">Gaming</a>
            </li>
            <li>
                <a href="/finance" class="text-lg truncate mr-5 top-menu @if($page == 'finance') top-menu--active font-medium @endif">Finance</a>
            </li>
			<li>
            	<a href="/news" class="text-lg truncate mr-5 top-menu @if($page == 'news') top-menu--active font-medium @endif">News</a>
            </li>
            <li>
                @livewire('search')
            </li>

            <div style="position: absolute; right:0; display: inline-flex;">
                <li>
                <a href="/random" class="text-lg truncate mr-5 top-menu"><img src="/images/remote-icon-2.png" style="width: 30px; opacity: 0.8;" title="Play Random Stream" id="flip-channel"></a>
            </li>
            
            @if (Route::has('login'))
                @auth
                    @if(Auth::user()->id == 1)
                        <li class="pull-right">
                            <a href="/signin" class="text-lg truncate mr-5 top-menu">Log in</a>
                        </li>
                    @else
                    <li class="pull-right">
                        <a href="/private" class="text-lg truncate mr-5 top-menu @if($page == 'private') top-menu--active font-medium @endif">Private</a>
                    </li>
                    <li class="pull-right">
        				<a href="/" href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="text-lg truncate mr-5 top-menu">Logout</a>
        			</li>
                    @endif
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @else
                
                    <li>
                        <a href="{{ route('login') }}" class="text-lg truncate mr-5 top-menu">Log in</a>
                    </li>
                    <li>

                @endauth
            @endif
        </li>
		</ul>
	</div>