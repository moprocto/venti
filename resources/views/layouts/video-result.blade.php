@if(!$video->blocked())
<div class="intro-y blog col-span-12 lg:col-span-4 md:col-span-4 box">
    <div class="blog__preview image-fit">
        <img alt="Midone Tailwind HTML Admin Template" class="rounded-t-md" src="{{ $video['thumbnail'] }}">
        <div class="absolute w-full flex items-center px-5 pt-6 z-10">
        </div>
        <div class="absolute bottom-0 text-white px-5 pb-6 z-10">
            <a href="/event/{{ $video->iframe }}/{{ $video->source }}" class="block font-medium text-xl mt-3">{{ $video["title"] }}</a>
        </div>
    </div>
    <div class="text-center lg:text-right p-5 border-t border-gray-200 dark:border-dark-5">
        <a class="button button--md text-white bg-theme-1 mr-2" href="/event/{{ $video->iframe }}/{{ $video->source }}">Join Watch Party</a>
        @auth
            @if(Auth::user()->id == 6)
                <button id="block" data-channel="{{ $video->channel }}">Block</button>
            @endif
        @endauth
    </div>
</div>
@endif