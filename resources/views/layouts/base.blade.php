<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="dark">
    <head>
        <meta charset="utf-8">
        <link href="/images/logo.png" rel="shortcut icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="description" content="Watch all the internet's live streams with everyone no matter where they are!">
        <meta name="keywords" content="live videos with chat">
        <meta name="author" content="venti">
        <title>venti: watch it live with everyone, everywhere</title>
        <meta property="og:description" content="Watch all the internet's live streams with everyone no matter where they are!" />
        <meta property="og:image" content="https://venti.co/images/venti-banner.png" />
        <!-- Favicon -->

        <!-- Fonts -->
        <link rel="stylesheet" href="https://rsms.me/inter/inter.css">

        <!-- Styles -->
        <link rel="stylesheet" href="/css/theme.css">
        <link rel="stylesheet" href="{{ url(mix('css/app.css')) }}">
        
        @livewireStyles

        <!-- Scripts -->
        <script src="{{ url(mix('js/app.js')) }}" defer></script>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <style type="text/css">
            @media (max-width: 1279px){
                .login {
                    background: none;
                }
            }
        </style>
        @yield('css')
    </head>

    <body class="login">
        @yield('body')

        @livewireScripts
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-Z6MV4F6SMV"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-Z6MV4F6SMV');
        </script> 
        <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js' id='jquery-core-js'></script>
        <script src="/js/theme.js"></script>
        @yield('js')
    </body>
</html>
