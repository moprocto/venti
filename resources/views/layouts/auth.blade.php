@extends('layouts.base')

@section('css')
	<style>
		.login-form{
			margin-top:8em;
		}
        @media (max-width: 1279px)
            .login {
                background-color: inherit !important;
            }
        }
	</style>
@endsection

@section('body')
    <div class="container sm:px-10">
        <div class="block xl:grid grid-cols-2 gap-4">
            <!-- BEGIN: Register Info -->
            <div class="hidden xl:flex flex-col min-h-screen">
                <a href="/" class="-intro-x flex items-center pt-5">
                    <img alt="Midone Tailwind HTML Admin Template" class="w-10" src="/images/logo.png">
                    <span class="text-white text-lg ml-3">
                    </span>
                </a>
                <div class="my-auto">
                    <img alt="Midone Tailwind HTML Admin Template" class="-intro-x  -mt-16" src="/images/chillin5.png" style="max-width: 400px;">
                    <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">Watch it live<br> with everyone else</div>
                </div>
            </div>
            @yield('content')

	        @isset($slot)
	            {{ $slot }}
	        @endisset
        </div>
    </div>
@endsection
