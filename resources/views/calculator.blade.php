<div class=" text-left">
	<div class="sec-heading center">
                <h2>Calculate Your Payout</h2>
              </div>
	<div class="col-md-12 mb-4 ">
		<h5>Select Pool Entry Amount:</h5>
		<input type="text" id="pool-amount" class="calculate" 
          data-provide="slider"
          data-slider-ticks="[0, 1, 2, 3, 4, 5, 6]"
          data-slider-ticks-labels='["$0", "$10", "$20", "$50", "$100", "$500", "$1,000"]'
          data-slider-min="1"
          data-slider-max="6"
          data-slider-step="1"
          data-slider-value="3"
          data-slider-tooltip="hide" />
    </div>
    <div class="col-md-12 mb-4">
		<h5>Select Number of Participants</h5>
		<input type="text"  id="pool-size" class="calculate" 
          data-provide="slider"
          data-slider-ticks="[20, 40, 60, 80, 100]"
          data-slider-ticks-labels='["20", "40", "60", "80", "100"]'
          data-slider-min="20"
          data-slider-max="100"
          data-slider-step="20"
          data-slider-value="100"
          data-slider-tooltip="hide" />
    </div>
    <div class="col-md-12 mb-4">
		<h5>Select Exam Pass Rate</h5>
		<input type="text"  id="pool-rate" class="calculate" 
          data-provide="slider"
          data-slider-ticks="[0.4, 0.5, 0.6, 0.7, 0.8, 0.9]"
          data-slider-ticks-labels='["40%", "50%", "60%", "70%", "80%", "90%"]'
          data-slider-min="0.4"
          data-slider-max="0.9"
          data-slider-step="0.1"
          data-slider-value="0.4"
          data-lock-to-ticks="true"
          data-slider-tooltip="hide" />
    </div>
    <div class="col-md-12">
    	<p>Assuming you pass, you will split <strong id="pool-prize">$5,000</strong> with <strong id="pool-split">40</strong> people. Your payout will be:</p>
    	<h2>$<span id="pool-payout">125.00</span></h2>
    	<!-- <p>A profit of $<strong id="pool-profit">75.00</strong> before fees.</p> -->
    </div>
</div>