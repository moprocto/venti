<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1' />
    <meta name="author" content="venti">
    <meta property="og:image" content="https://venti.co/images/metaimage-venti.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,500,600">
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/fontawesome.css" integrity="sha384-eHoocPgXsiuZh+Yy6+7DsKAerLXyJmu2Hadh4QYyt+8v86geixVYwFqUvMU8X90l" crossorigin="anonymous"/>
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/css/style.css">
  <title>venti | A personal voice assistant for your website or app</title>
  <link rel="icon" 
      type="image/png" 
      href="/images/favicon.png">
  <style type="text/css">
  .w-100{
        min-width: 125px !important;
    }
  .text-right{

    text-align: right;
  }
  @media only screen and (max-width: 600px) {
    .rotation-c, .in-rotate{
        display: none !important;
    }
  }
  @media only screen and (min-width: 500px) {
      .rotation-c{
        position: fixed;
        
  animation-timing-function: linear;
  animation-iteration-count: infinite;
        }

      .in-rotate {
        animation: rotation 100s linear infinite !important;
        left: 80%;
        top: 60%;
        width: 300px;
        height: 200px;
}

.cc-rotate {
        animation: cc-rotation 200s linear infinite !important;
        left: 40%;
        top: -20%;
        width: 300px;
        height: 300px;

      
    
}

@keyframes rotation {
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
}
@keyframes cc-rotation {
  from {
    transform: rotate(360deg);
  }
  to {
    transform: rotate(0deg);
  }
}
}
.ts-background-image{
z-index: 1000;
position:absolute;
}


  </style>

</head>
<body class="ts-theme-dark">

    <!--*************************************************************************************************************-->
    <!--************ MAIN SCREEN ************************************************************************************-->
    <!--*************************************************************************************************************-->

    <div class="ts-page-wrapper">
        <div class="container">

            <!--NAVIGATION ******************************************************************************************-->
            <header id="header" >
                <nav class="navbar navbar-dark ts-separate-bg-element">
                    <a class="navbar-brand" href="#">venti</a>
                    <!--end navbar-brand-->
                    <button class="navbar-toggler ts-open-side-panel" type="button" aria-label="Toggle navigation" style="display: none;">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <!--end navbar-toggler-->
                </nav>
                <!--end navbar-->
            </header>
            <!--end #header-->

            <!--CONTENT *********************************************************************************************-->
            <main id="main-content">
                <div class="ts-content-wrapper">
                    <div class="row">
                        <!--Page Title-->
                        <div class="col-md-6" style="margin:auto;">
                            <h1>A personal voice assistant <br> for your <span id="text-rotator"></span></h1>
                            We're launching soon
                            <form class="ts-form ts-form-email" data-php-path="/signup">
                                @CSRF
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="form-label text-small">Join Waitlist</label>
                                            <input type="email" class="form-control" id="email-subscribe" aria-describedby="subscribe" name="email" placeholder="Your email" required>
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <!--end col-md-10-->
                                    <div class="col-md-3">
                                        <label class="form-label text-small"></label>
                                        <button type="submit" class="btn btn-primary w-100">Add Me</button>
                                    </div>
                                    <!--end col-md-2-->
                                </div>
                                <!--end row-->
                            </form>
                            <span style="font-size:12px;">Let's stay in touch: <a href="mailto:markus@venti.co">markus@venti.co</a> (we're hiring)</span>
                            <!--end ts-form-->
                        </div>
                        <!--end col-md-6-->
                        <!--Count Down-->
                        <div class="col-md-6">
                            <div class="text-right my-4">
                                <img src="/images/hero-venti.png" style="width: 100%; max-width:600px;">
                            </div>
                        </div>
                        <!--end col-md-6-->
                    </div>
                    <!--end row-->
                </div>
                <!--end ts-content-wrapper-->
            </main>
            <!--end #main-content-->


        </div>
        <!--end container-->


            <div class="in-rotate rotation-c" style="opacity:0.3">
              <img class="ts-background-image ts-animate ts-bounce" src="/images/blob-7.png" style="width: 300px; height:200px;">
            </div>
            <div class="cc-rotate rotation-c" style="opacity:0.3;">
              <img class="ts-background-image ts-animate ts-bounce" src="/images/blob-1.png" style="width: 300px; height:300px;" data-bg-opacity=".8">
            </div>
        <!--BACKGROUND **********************************************************************************************-->
        <div class="ts-background ts-shapes-canvas position-fixed ts-separate-bg-element" data-bg-color="#00265f">
            <div class="ts-background-image ts-animate ts-scale" data-bg-image="/images/bg/14.png"></div>
            <div class="ts-background-image ts-animate ts-bounce" data-bg-image="/images/bg/13.png"></div>
            <div class="ts-background-image ts-animate ts-scale" data-bg-image="/images/bg/12.png" data-bg-opacity=".3"></div>
            <div class="ts-background-image ts-animate ts-scale" data-bg-image="/images/bg/11.png"></div>
            <div class="ts-background-image ts-animate ts-bounce" data-bg-image="/images/bg/10.png"></div>
            <div class="ts-background-image ts-animate ts-bounce" data-bg-image="/images/bg/9.png" data-bg-opacity=".8"></div>
            <div class="ts-background-image ts-animate ts-bounce" data-bg-image="/images/bg/8.png" data-bg-opacity=".8"></div>
            
            <div class="ts-background-image ts-animate ts-bounce" data-bg-image="/images/bg/6.png" data-bg-opacity=".8"></div>
            <div class="ts-background-image ts-animate" data-bg-image="/images/bg/5.png" data-bg-opacity=".8"></div>
            <div class="ts-background-image ts-animate ts-bounce" data-bg-image="/images/bg/4.png"></div>
            <div class="ts-background-image ts-animate ts-bounce" data-bg-image="/images/bg/3.png" data-bg-opacity=".8"></div>
            <div class="ts-background-image ts-animate ts-bounce" data-bg-image="/images/bg/2.png"></div>
            
        </div>
        <!--end ts-background-->
    </div>
    <!--end ts-page-wrapper-->

    <!--*************************************************************************************************************-->
    <!--************ SIDE PANEL *************************************************************************************-->
    <!--*************************************************************************************************************-->

    <div class="ts-side-panel" data-bg-color="#001330" style="display:none;">
        <a href="#" class="ts-close-side-panel">
            <i class="far fa-times-circle"></i>
        </a>
        <!--end ts-close-side-panel-->
        <div class="container-fluid">
            <section>
                <div class="ts-title">
                    <h2>About Us</h2>
                </div>
                <!--end ts-title-->
                <p>
                    Uis commodo arcu at egestas vehicula. Maecenas auctor sagittis nulla laoreet vestibulum.
                    Vivamus congue diam blandit quam efficitur, vel mattis lacus feugiat. Integer tempus interdum felis,
                    ut luctus nunc.
                </p>
                <div class="row">
                    <div class="col-sm-6 col-xl-4">
                        <div class="d-flex align-items-center my-3">
                            <div class="ts-circle__md">
                                <figure class="ts-background-image" data-bg-image="/img/person-01.jpg"></figure>
                            </div>
                            <div class="ml-3">
                                <h3 class="mb-0">Jane Doe</h3>
                                <h5 class="ts-opacity__50">Company CEO</h5>
                            </div>
                        </div>
                        <!--end d-flex-->
                    </div>
                    <!--end col-md-4-->
                    <div class="col-sm-6 col-xl-4">
                        <div class="d-flex align-items-center my-3">
                            <div class="ts-circle__md">
                                <figure class="ts-background-image" data-bg-image="/img/person-02.jpg"></figure>
                            </div>
                            <div class="ml-3">
                                <h3 class="mb-0">Peter Browns</h3>
                                <h5 class="ts-opacity__50">Marketing Guru</h5>
                            </div>
                        </div>
                        <!--end d-flex-->
                    </div>
                    <!--end col-md-4-->
                    <div class="col-sm-6 col-xl-4">
                        <div class="d-flex align-items-center my-3">
                            <div class="ts-circle__md">
                                <figure class="ts-background-image" data-bg-image="/img/person-04.jpg"></figure>
                            </div>
                            <div class="ml-3">
                                <h3 class="mb-0">Susane Erwing</h3>
                                <h5 class="ts-opacity__50">Support</h5>
                            </div>
                        </div>
                        <!--end d-flex-->
                    </div>
                    <!--end col-md-4-->
                </div>
                <!--end row-->
            </section>
            <!--end section-->
            <section>
                <div class="ts-title">
                    <h2>Features</h2>
                </div>
                <!--end ts-title-->
                <div class="row">
                    <div class="col-md-4">
                        <img src="/img/icon-wallet.png" alt="" class="mw-100 mb-4">
                        <h3>Easy Payments</h3>
                        <p>
                            Aenean ut orci mattis, rhoncus velit interdum, molestie mi
                        </p>
                    </div>
                    <!--end col-md-4-->
                    <div class="col-md-4">
                        <img src="/img/icon-cloud.png" alt="" class="mw-100 mb-4">
                        <h3>Secure Cloud</h3>
                        <p>
                            Uis commodo arcu at egestas vehicula. Maecenas auctor
                        </p>
                    </div>
                    <!--end col-md-4-->
                    <div class="col-md-4">
                        <img src="/img/icon-laptop.png" alt="" class="mw-100 mb-4">
                        <h3>Responsive</h3>
                        <p>
                            Integer tempus interdum felis, ut luctus nunc.
                        </p>
                    </div>
                    <!--end col-md-4-->
                </div>
                <!--end row-->
            </section>
            <!--end section-->
            <section>
                <div class="ts-title">
                    <h2>Our Mission</h2>
                </div>
                <!--end ts-title-->
                <div class="row">
                    <div class="col-md-12 col-xl-4">
                        <p>
                            Nam rhoncus elit ut nibh sagittis varius. Maecenas aliquam ex ut magna cursus pulvinar
                        </p>
                        <ul>
                            <li>Nulla facilisi</li>
                            <li>Donec fringilla dui</li>
                            <li>Dapibus, ut placerat</li>
                        </ul>
                    </div>
                    <!--end col-xl-4-->
                    <div class="col-md-12 col-xl-8">
                        <img src="/img/image-01.jpg" alt="" class="mw-100">
                    </div>
                    <!--end col-xl-8-->
                </div>
                <!--end row-->
            </section>
            <!--end section-->
            <section>
                <div class="ts-title">
                    <h2>Contact</h2>
                </div>
                <!--end ts-tile-->
                <h3>Map</h3>
                <div class="map ts-height__200px mb-5" id="map"></div>
                <!--end map-->
                <div class="row">
                    <div class="col-xl-4">
                        <h3>Address</h3>
                        <address>
                            4758 Nancy Street
                            <br>
                            +1 919-571-2528
                            <br>
                            <a href="#">hello@example.com</a>
                        </address>
                    </div>
                    <!--end col-xl-4-->
                    <div class="col-xl-8">
                        <h3>Contact Form</h3>
                        <form id="form-contact" class="ts-form ts-form-email ts-inputs__transparent" data-php-path="/php/email.php">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="form-contact-name" name="name" placeholder="Your Name" required>
                                    </div>
                                    <!--end form-group -->
                                </div>
                                <!--end col-md-6 col-sm-6 -->
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="form-contact-email" name="email" placeholder="Your Email" required>
                                    </div>
                                    <!--end form-group -->
                                </div>
                                <!--end col-md-6 col-sm-6 -->
                            </div>
                            <!--end row -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" id="form-contact-message" rows="6" name="message" placeholder="Your Message" required></textarea>
                                    </div>
                                    <!--end form-group -->
                                </div>
                                <!--end col-md-12 -->
                            </div>
                            <!--end row -->
                            <div class="form-group clearfix">
                                <button type="submit" class="btn float-right btn-outline-primary" id="form-contact-submit">Send a Message</button>
                            </div>
                            <!--end form-group -->
                            <div class="form-contact-status"></div>
                        </form>
                        <!--end form-contact -->
                    </div>
                    <!--end col-xl-8-->
                </div>
                <!--end row-->
            </section>
            <!--end section-->
        </div>
        <!--end container-fluid-->
    </div>
    <!--end ts-side-panel-->
    <div class="container" style="background:#021737; padding: 2em; padding-top: 1em;">
        <div class="row">
            <div class="col-md-12 text-center">
                <img src="/images/venti-orb-final.png" width="100">
                <h2>Integrations</h2>
                Allow users to purchase, search, and share feedback on your site or app using just their voice. <br> venti can be configured and deployed in as little as five minutes with no coding required.<br>
            </div>
        </div>
        <div class="row" style="margin-top:45px;">
            <div class="col-md-3"><img src="/images/wordpress.png" style="width:100%;"></div>
            <div class="col-md-3"><img src="/images/shopify.png" style="width:100%;"></div>
            <div class="col-md-3"><img src="/images/react.png" style="width:100%;"></div>
            <div class="col-md-3"><img src="/images/xcode.png" style="width:100%;"></div>
        </div>
    </div>
    <!--*************************************************************************************************************-->
    <!--************ JS SCRIPTS *************************************************************************************-->
    <!--*************************************************************************************************************-->

  <script src="/js/jquery-3.3.1.min.js"></script>
  <script src="/js/popper.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
    <script src="/js/imagesloaded.pkgd.min.js"></script>

    <script src="/js/jquery.magnific-popup.min.js"></script>
  <script src="/js/jquery.countdown.min.js"></script>
  <script src="/js/jquery.validate.min.js"></script>
  <script src="/js/jquery-validate.bootstrap-tooltip.min.js"></script>

    <script src="/js/custom.js"></script>

    <script>
        jQuery(document).ready(function ($) {
            displayClass(0);

            function displayClass(i) {
            var classes = [
              'online store',
              'blog',
              'mobile app',
              'web app'
            ];

            $('#text-rotator').text(classes[i]).fadeIn(1000, function() {
                $(this).delay(600).fadeOut(1000, function() {
                    displayClass((i + 1) % classes.length);
                });
            });
          }
        });
  </script>

@if(env('APP_ENV') == "production")
    <!-- Hotjar Tracking Code for https://venti.co -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2651983,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
      <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-Z6MV4F6SMV"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-Z6MV4F6SMV');
</script>
    @endif

</body>
</html>
