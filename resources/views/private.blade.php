@extends('layouts.master')

@section('css')
		div.content{
            border-top-left-radius: 30px !important;
        }
        html, body{
        background-color:#222b3b !important;
    }
    @media only screen and (max-width: 600px) {
	    .blog__preview div.absolute a.block{
		    text-overflow: ellipsis;
		    width: 80vw;
		    overflow: hidden;
		}
	}
@endsection

@section('content')
    <h2>A private room for your and your friends to watch anything together</h2>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/dp8PhLsUcFE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
@endsection