<section class="min gray">
        <div class="container">
          
          <div class="row justify-content-center">
            <div class="col-lg-7 col-md-8">
              <div class="sec-heading center">
                <h2>The <span class="theme-cl">Benefits </span>of Joining a Study Pool</h2>
                <p style="font-size: 18px;">Regardless of which pool you join or how much you put in, you get the following:</p>
              </div>
            </div>
          </div>
          
          <div class="row justify-content-center mt-5">
            
            <!-- Single Location -->
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="wrk_grid">
                <div class="wrk_grid_ico">
                  <img src="/images/exam-icon.png" style="width: 60%;">
                </div>
                <div class="wrk_caption">
                  <h4>Hundres of Exam Questions</h4>
                  <p>So you can practice until you can't get it wrong. Do them alone or form study groups with others in your pool. <strong>Limited question set with free pools.</strong></p>
                </div>
              </div>
            </div>
            
            <!-- Single Location -->
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="wrk_grid">
                <div class="wrk_grid_ico">
                  <img src="/images/teaching.png" style="width: 60%;">
                </div>
                <div class="wrk_caption">
                  <h4>Live Coaching Sessions</h4>
                  <p>With your pool so you can review important concepts and tricky test questions. <strong>Not available with free pools.</strong></p>
                </div>
              </div>
            </div>
            
            <!-- Single Location -->
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="wrk_grid">
                <div class="wrk_grid_ico">
                  <img src="/images/users.png" style="width: 60%;">
                </div>
                <div class="wrk_caption">
                  <h4>Accountability</h4>
                  <p>Be surrounded by focused professionals that are just as serious about passing the exam as you.</p>
                </div>
              </div>
            </div>
            
          </div>
          
        </div>
      </section>