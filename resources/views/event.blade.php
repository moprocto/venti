@extends('layouts.master')

@section('css')
.content{
    padding-left:0;
    padding-right:0;
}
body{
    background-color: #283143 !important;
}
.dark .content{
    background:none !important;
}
div.stretched{
    width:100% !important;
    grid-column: span 12/span 12 !important;
}

.owner{
    float:right;
    clear:both;
}

.box .owner .bg-gray-200 , .box .owner .bg-gray-200 {
    background-color:#0E85FF !important;
}

.chat__box__text-box {
    max-width: 49%;
    text-align:left;
}
.float-left{
    clear:both;
}
.float-right{
    
}
.items-end {
    align-items: flex-end;
}

/* mobile updates */

@media only screen and (max-width: 600px) {
    .overflow-y-scroll{
        padding-left:0 !important; 
        padding-right:0 !important;
    }
    iframe.yt-player{
        height: 53vw !important;
    }
    img.no-messages{
        margin-bottom: 77px !important;
    }
}
@endsection

@section('content')

<div class="intro-y col-span-12 lg:col-span-8 xxl:col-span-9" id="player-box">
    @if($source == 1)
        <iframe src="https://www.youtube.com/embed/{{ $id }}?autoplay=true&muted=false" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" class="yt-player" allowfullscreen style="width: 100%; height: 40vw;" autoplay="true"></iframe>
    @endif
    @if($source == 2)
        <iframe
            src="https://player.twitch.tv/?channel={{ $channel }}&video={{ $id }}&parent=venti.co&autoplay=true&muted=false"
            style="width: 100%; height: 40vw;"
            allowfullscreen="true">
        </iframe>
    @endif

    @if($source == 3)
        <iframe src="https://www.dailymotion.com/embed/video/{{ $id }}" style="border:none;overflow:hidden; width: 100%; height: 40vw;" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true" autoplay="autoplay"></iframe>
    @endif
    
    <br>
    <div class="intro-y col-span-12 lg:col-span-4 xxl:col-span-12">
        <div class="intro-y box col-span-12 lg:col-span-6">
            <div class="flex items-center px-5 py-5 sm:py-0 border-b border-gray-200 dark:border-dark-5">
                <h2 class="font-medium text-base mr-auto">{{ $title }}</h2>
                <div class="nav-tabs ml-auto hidden sm:flex">
                    <a href="javascript:;" class="py-5 ml-6 stretch-player" id="maximize-icon"><i data-feather="maximize-2" class="maximize-icon"></i><i data-feather="minimize-2" class="hidden minimize-icon"></i></a>
                </div>
            </div>
            <div class="accordion__pane active border border-gray-200 dark:border-dark-5 p-4">
                <a href="javascript:;" class="accordion__pane__toggle font-medium block">Description</a>
                @auth
                    @if(Auth::user()->id == 6)
                        <button id="delete" class="btn button">Delete</button>
                    @endif
                @endauth
                <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed" style="width:100%; overflow-wrap: break-word;">{{ $description }}</div>
            </div>
        </div>
    </div>
</div>

@livewire('chat-room', ["room" => "1"])

@endsection

@section('js')
    <script>
        jQuery(document).ready(function ($) {
            feather.replace();

            userHasScrolled = false;

            $("body").on("click", ".stretch-player", function(){
                $("#player-box").toggleClass("stretched");
                $('.maximize-icon').toggle();
                $('.minimize-icon').toggle();

                $("#chat-box").toggleClass("stretched");
            });

            function resetToBottom(){
                $(".chat__box .overflow-y-scroll").stop().animate({ scrollTop: $(".chat__box .overflow-y-scroll")[0].scrollHeight}, 1000);
            }

            resetToBottom();

            var intervalId = window.setInterval(function(){
              /// call your function here

             var scrollPosition = $(".overflow-y-scroll").scrollTop(); //check the number in console

              if(scrollPosition >= $(".overflow-y-scroll")[0].scrollHeight * 0.65 && !userHasScrolled){
                resetToBottom();
                userHasScrolled = false;
              }

            }, 1000);

            $(".overflow-y-scroll").scroll(function(){ 
                // called when the window is scrolled.  
                userHasScrolled = true;
                console.log("scrolling");
            });

            $("body").on("click", "#delete", function(){

                $.get( "/hooks/delete/video/{{ $id }}", function( data ) {
                  
                });

                window.location.href = "/random";
            });
        });
    </script>
@endsection
