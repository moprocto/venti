<div class="container">
          <div class="row align-items-center justify-content-end">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
              <div class="sec-heading center">
                <h2>The Bold Print</h2>
              </div>
              <div class="lmp_caption">
                <ol class="list-unstyled p-0">
                  <li class="d-flex align-items-start my-3 my-md-4">
                  <div class="rounded-circle p-3 p-sm-4 d-flex align-items-center justify-content-center gray">
                    <div class="position-absolute theme-cl h5 mb-0"><i class="fa fa-question"></i></div>
                  </div>
                  <div class="ml-3 ml-md-4">
                    <h4>What if I fail the exam?</h4>
                    <p>You lose your pool entry deposit if you fail your exam. If you do not provide proof of passing your exam <strong>within 120 days</strong> of the pool expiry date, you forfeit your deposit because we will assume you failed.</p>
                  </div>
                  </li>
                  <li class="d-flex align-items-start my-3 my-md-4">
                  <div class="rounded-circle p-3 p-sm-4 d-flex align-items-center justify-content-center gray">
                    <div class="position-absolute theme-cl h5 mb-0"><i class="fa fa-clock"></i></div>
                  </div>
                  <div class="ml-3 ml-md-4">
                    <h4>How long does it take to receive my payout?</h4>
                    <p>It depends on the exam you complete and when your credential gets issued. You must provide your <strong>Credential ID</strong> for us to verify that you actually passed your exam. We aim for deposits to reach you within 120 days of the pool expiry date.</p>
                  </div>
                  </li>
                   <li class="d-flex align-items-start my-3 my-md-4">
                  <div class="rounded-circle p-3 p-sm-4 d-flex align-items-center justify-content-center gray">
                    <div class="position-absolute theme-cl h5 mb-0"><i class="fa fa-dollar-sign"></i></div>
                  </div>
                  <div class="ml-3 ml-md-4">
                    <h4>How much are the fees?</h4>
                    <p>To cover our bills, we plan to charge a 2% fee from the winnings of those that passed. <strong>Fees are subjected to change.</strong></p>
                  </div>
                  </li>
                  <li class="d-flex align-items-start my-3 my-md-4">
                  <div class="rounded-circle p-3 p-sm-4 d-flex align-items-center justify-content-center gray">
                    <div class="position-absolute theme-cl h5 mb-0"><i class="fa fa-calendar"></i></div>
                  </div>
                  <div class="ml-3 ml-md-4">
                    <h4>Can I drop out of a pool?</h4>
                    <p>You can change pools at anytime before the pool expiry. There is a small fee associated with changing pools, so pick the pool the best matches when you plan to take the exam. If you decide to no longer pursue your exam, you must drop out within seven days of joining a pool and before the pooly expiry to get your deposit back.</p>
                  </div>
                  </li>
                </ol>
              </div>
              <div class="lmp_caption col-md-12 mb-4">
                <div class="text-left mt-4"><a href="https://apptestingco.typeform.com/to/lxXxBGfa" target="_blank" class="btn btn-md text-light theme-bg">Join Wait List</a></div>
              </div>
            </div>
          </div>
        </div>