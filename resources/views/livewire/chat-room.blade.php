<div class="intro-y col-span-12 lg:col-span-4 xxl:col-span-9" id="chat-box">
    <div class="chat__box box" style="background: none; box-shadow: none;">
        <!-- BEGIN: Chat Active -->
        <div class="h-full flex flex-col" style="">
                <!-- live wire -->
                <div class=""
                    x-data="{{ json_encode(['messages' => $messages ] ) }}"
                    x-init="
                        messageBody = $('#messageBody').val();
                        Echo.join('demo')
                            .listen('MessageSentEvent', (e) => {
                                @this.call('incomingMessage', e),
                                $('.chat__box .overflow-y-scroll').stop().animate({ scrollTop: $('.chat__box .overflow-y-scroll')[0].scrollHeight}, 500);
                                
                                
                            })
                        ">
                        <template x-if="messages.length == 0">
                            <img src="/images/empty-chat.png" class="no-messages" style="position:absolute; width: 300px;  margin: 0 auto; left:50%; margin-left: -150px; bottom: 0; opacity: 0.3; 
                            @auth 
                                @if(Auth::user()->id == 1)
                                    margin-bottom: 52px;  
                                @else 
                                    margin-bottom: 67px;
                                @endif
                            @endauth
                            ">
                        </template>
                    <div class="overflow-y-scroll scrollbar-hidden px-5 pt-5 flex-1" style="height:38em;">
                    <template x-if="messages.length > 0"> 
                        <template
                            x-for="message in messages"
                            :key="message.id"
                        >
                            <div @auth :class="{ 'owner': message.user_id === {{ Auth::user()->id }} }" @endauth class="chat__box__text-box flex items-end mb-4 float-left">
                                <div class="bg-gray-200 dark:bg-dark-5 px-4 py-3 text-gray-700 dark:text-gray-300 rounded-r-md rounded-t-md">
                                        <span class="chat-block" x-text="message.user.name"></span><br><span class="" x-text="message.body"></span>
                                </div>
                            </div>
                        </template>
                    </template>
                    <template x-if="messages.length == 0">
                        <div class="py-4 text-white-600">
                            It's quiet in here...
                        </div>
                    </template>
                </div>
                <div class="pt-4 pb-10 sm:py-4 flex items-center border-t border-gray-200 dark:border-dark-5" style="clear:both;">

                    @guest
                            <p><a href="/signin">Sign-in</a> to join the chat</p>
                    @else
                        @if(Auth::user()->id == 1)
                            <p><a href="/signin">Sign-in</a> to join the chat</p>
                        @else

                        <input
                        
                            @keydown.enter="
                                @this.call('sendMessage', messageBody)
                                messageBody = ''
                                $('.chat__box .overflow-y-scroll').stop().delay(600).animate({ scrollTop: $('.chat__box .overflow-y-scroll')[0].scrollHeight}, 500);
                            "
                            id="messageBody"
                            x-model="messageBody"
                            class="chat__box__input input mr-4  appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
                            type="text"
                            placeholder="Say something... :)">

                        <button
                            @click="
                                @this.call('sendMessage', messageBody)
                                messageBody = ''
                            "
                            class="btn btn-primary self-stretch"
                        >
                            Send
                        </button>
                        @endif
                    @endguest
                </div>
                    @error('messageBody') <div class="error mt-2">{{ $message }}</div> @enderror
                </div>

            <!-- livewire -->

        </div>

    </div>
    
</div>
