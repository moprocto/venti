@section('title', 'Create a new account')

<div class="login-form" style="margin-top:10em;">
    <div class="sm:mx-auto sm:w-full sm:max-w-md">
        <h2 class="mt-6 text-3xl font-extrabold text-center text-white-900 leading-9">
            Create a new account
        </h2>

        <p class="mt-2 text-sm text-center text-gray-600 leading-5 max-w">
            Or
            <a href="{{ route('login') }}" class="font-medium text-white hover:text-white-500 focus:outline-none focus:underline transition ease-in-out duration-150">
                sign in to your account
            </a>
        </p>
    </div>

    <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-lg">
        <div class="px-4 py-8 sm:rounded-lg sm:px-10">
            <form method="POST" action="{{ route('registration') }}">
                @CSRF
                <div class="">
                    <label for="phone_number" class="block text-sm font-medium text-white-700 leading-5">
                        Cell Phone (real people for real chats)
                    </label>

                    <div class="mt-1 rounded-md shadow-sm">
                        <div id="phone_number"></div>
                        <input name="phone_number" id="phone_number_field" type="hidden" class="login__input input" />
                    </div>

                    @error('phone_number')
                        <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mt-6">
                    <label for="name" class="block text-sm font-medium text-white-700 leading-5">
                        Username
                    </label>

                    <div class="mt-1 rounded-md shadow-sm">
                        <input id="name" name="name" type="text" required autofocus class="login__input input appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('name') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror" />
                    </div>

                    @error('name')
                        <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

                

                <div class="mt-6">
                    <span class="block w-full rounded-md shadow-sm">
                        <button type="submit" class="flex justify-center w-full px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                            Register
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>

@section('css')
<link href="/css/pinlogin.css" rel="stylesheet" type="text/css" />
@endsection

@section('js')
<script src="/js/pinlogin.min.js"></script>
<script>
    jQuery(document).ready(function ($) {
        
        var pinlogin = $("#phone_number").pinlogin({
            fields: 10,
            hideinput: false,
            reset: false,
            complete: function(pin){
                $("#phone_number_field").val(pin);
                console.log(pin);
            }
        });

        $("body").find("#phone_number_pinlogin_2").after("<span class='text-bold'>―</span>");
        $("body").find("#phone_number_pinlogin_5").after("<span class='text-bold'>―</span>");
    });
</script>
@endsection
