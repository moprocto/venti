<div class="search menu" style="10px; max-width: 350px;">
	<input type="text" class="search__input input placeholder-theme-13" placeholder="Search..." style="max-width: 350px;" wire:model="searchTerm" />
	@if($searchTerm != "")
		<style> @media only screen and (max-width: 600px) { .search-holder{padding-bottom:200px;} }</style>
		<div class="search-result @if($searchTerm != '') show @endif" >
			<div class="search-result__content">
    			<div class="mb-5">
					@foreach($events as $event)   
	                    @if($event['source'] == 2)
			                <a href="/event/{{ $event['channel'] }}/2" class="flex items-center mt-2">
			                    <div class="" style="width:100px; display: contents;">
			                        <img alt="Midone Tailwind HTML Admin Template" src="{{ $event['thumbnail'] }}" style="width: 100px;">
			                    </div>
			                    <div class="ml-3">{{ $event["title"] }}</div>
			                </a>
			            @else
			                <a href="/event/{{ $event['iframe'] }}/1" class="flex items-center mt-2">
			                    <div class="" style="width:100px; display: contents;">
			                        <img alt="Midone Tailwind HTML Admin Template" src="{{ $event['thumbnail'] }}" style="width: 100px;">
			                    </div>
			                    <div class="ml-3">{{ $event["title"] }}</div>
			                </a>
			            @endif
					@endforeach
					@if(sizeof($events) == 0)
						<h3>No results</h3>
					@endif
				</div>
			</div>
		</div>
	@endif
</div>