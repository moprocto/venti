<?php

use Alaouy\Youtube\Facades\Youtube as Youtube;
use App\Models\Event as Events;

    function parseTwitchThumbnailURL($string){
        $string = str_replace("{width}", "320", $string);
        $string = str_replace("{height}","180", $string);

        return $string;
    }