<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Twilio\Rest\Client;
use Redirect;
use Session;

class RegisterController extends Controller
{
    public function add(Request $request){
    	$this->validate($request, [
            'name' => ['required'],
            //'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'min:8'],
            'phone_number' => ['required', 'numeric', 'unique:users'],
        ]);

        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $twilio->verify->v2->services($twilio_verify_sid)
            ->verifications
            ->create("+1" . $request->phone_number, "sms");

        $user = User::create([
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        Auth::login($user, true);

        return redirect()->route('verify')->with(['phone_number' => $request->phone_number, "user" => $user]);
    }

    public function getVerify(){

    	return view('livewire.auth.verify');
    }

    protected function confirmSMS(Request $request)
    {
        $data = $request->validate([
            'verification_code' => ['required', 'numeric']
        ]);
        /* Get credentials from .env */
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $verification = $twilio->verify->v2->services($twilio_verify_sid)
            ->verificationChecks
            ->create($request->verification_code, array('to' => "+1" . Auth::user()->phone_number));
        if ($verification->valid) {
            $user = tap(User::where('phone_number', $request->phone_number))->update(['isVerified' => true]);
            /* Authenticate user */
            return redirect()->route('home')->with(['message' => 'Phone number verified']);
        }
        return back()->with(['error' => 'Invalid verification code entered!']);
    }
}
