<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Searchable\Search;
use App\Models\Event as Events;
use App\Models\Phrase as Phrases;
use DB;
use Excecption;

class SearchController extends Controller
{


	public function phrases(){
		$phrases = Phrases::where('id', '>', 0)->get()->toArray();

		foreach ($phrases as $phrase) {
			
			$spaces = substr_count($phrase["content"], ' ');
			if($spaces == 0){
				$words = explode(" ", $phrase["content"]);
			} else {
				$words = explode("-", $phrase["content"]);
			}
			
			$finalPhrase = "";
			foreach ($words as $word) {
				$length = strlen($word);


				if($length > 2){
					for($i = 0; $i < $length; $i++){
						if($word[$i] != "-" && $word[$i] != " "){
							if($i  > 0 && $i != $length - 1){
								if($length > 4 && $i < 4 && $length < 8){
									$word[$i] = "*";
								}
								if($length > 8 && $i < 8 && $length < 12){
									$word[$i] = "*";
								}
								if($length > 12 && $i < 12){
									if($i != 8){
										$word[$i] = "*";
									}
								}
							}
						}
					}
				}

				$finalPhrase .= " " . $word;

			}
			echo "-->" . $finalPhrase . " " . $phrase["id"] . "<br>";

			try{
				DB::table('phrases')->where('id', '=', $phrase["id"])->update(["swap" => utf8_encode ($finalPhrase)]);
			} catch(Excecption $e){
				// DB::table('phrases')->where('id', '=', $phrase["id"])->delete());
			}
			
			$finalPhrase = "";
			usleep(200);

			
		}
	}
}
