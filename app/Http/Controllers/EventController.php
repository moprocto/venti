<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event as Events;
use App\Models\User as Users;
use App\Models\Blocked as Blocked;
use Alaouy\Youtube\Facades\Youtube as Youtube;
use romanzipp\Twitch\Twitch as Twitch;
use Session;
use Auth;
use Exception;
use Redirect;

class EventController extends Controller
{
    public function browse($category = "top"){

        if($category != "top" && $category != ""){
            $videos = Events::inRandomOrder()->where("category", $category)->orderBy('views', "DESC")->get();
        } else {
            $videos = Events::orderBy('views', "DESC")->limit(100)->get();
        }

        $data = [
            "videos" => $videos,
            "page" => $category
        ];

        return view('browse', $data);
    }

    public function privateRooms(){

        $videos = Events::where('type', 2)->get();

        $data = [
            "videos" => $videos,
            "page" => "private"
        ];

        return view('private', $data);
    }

    public function read($id, $source){
        Session::forget("room");

        $video = Events::where('iframe', $id)->first();

        if(is_null($video) && $source == 2){
            $video = Events::where('channel', $id)->first();
        }
        

        if(!Auth::check()){
            $user = Users::find(1);
            Auth::login($user, true);
        }
        
        Session::put("room", $id . "-1");

    	$data = [
            "id" => $id,
            "title" => $video["title"],
            "description" => $video["description"],
            "thumbnail" => $video["thumbnail"],
            "source" => $video["source"],
            "channel" => $video["channel"],
            "page" => NULL
        ];

        return view('event', $data);
    }

    public function random(){
        $video = Events::inRandomOrder()->where('views', '>', 100)->first();

        return Redirect::to('/event/' . $video->iframe . '/' . $video->source);
    }

    public function edit(){
    	
    }

    public function add(){

    }

    public function delete($id){
    	$event = Events::where('iframe', $id)->first();
        $event->delete();
    }

    public function search(Request $request){
        $results = Youtube::searchAdvanced(
            [
                'q' => "live " . $request->q, 
                'type' => 'live',
                'part' => 'id, snippet',
                'maxResults'    => 50
            ]
        );

        $videos = [];
        
        foreach ($results as $result) {
            if(isset($result->id->videoId)){
                array_push($videos, [
                    "id" => $result->id->videoId,
                    "title" => $result->snippet->title,
                    "description" => $result->snippet->description,
                    "thumbnail" => $result->snippet->thumbnails->medium->url,
                    "type" => "youtube"
                ]);
            }
        }

        $data = [
            "videos" => $videos
        ];

        return view('browse', $data);
    }

    public function queryVideoFeed($category, $loopLimit){

        $videos = [];

        if($category == "finance"){
            $category = "economy";
        }

        $params = [
            'q' => "$category", 
            'type' => 'video',
            'eventType' => 'live',
            'part' => 'id, snippet',
            'maxResults'    => 50
        ];

        if($category == "economy"){
            $category = "finance";
        }


        for($i = 0; $i < $loopLimit; $i++){
            try{
                 $results = Youtube::searchAdvanced($params, true);
            } catch(Exception $e) {
               // couldn't get the YouTube API to load... deliver something to user
               if($category == "music" || $category == "news" || $category == "finance"){
                    $events = Events::where('source', 1)->where('category', $category)->orderBy('views', "DESC")->get();

                    foreach ($events as $event) {
                       array_push($videos, [
                            "id" => $event->iframe,
                            "title" => $event->title,
                            "thumbnail" => $event->thumbnail,
                            "type" => $event->source,
                            "views" => $event->views,
                            "channel" => $event->channel,
                            "description" => NULL
                        ]);
                    }

                    return $videos;
               }
               

                break;
            }
           
            foreach ($results as $result) {
                if(is_array($result)){
                    foreach ($result as $key => $video) {
                        if(isset($video->id->videoId)){
                            $id = $video->id->videoId;
                            if(Blocked::where('channel', $video->snippet->channelId)->count() == 0){

                            if($video->snippet->liveBroadcastContent == "live"){
                                try{
                                    $video = Youtube::getVideoInfo($id);
                                } 
                                catch(Exception $e){
                                    break;
                                }
                                
                                $lang = false;



                                if(isset($video->snippet->defaultLanguage)){
                                    if($video->snippet->defaultLanguage == "en"){
                                        $lang = true;
                                    } else{
                                        $lang = false;
                                    }
                                }

                                 if(isset($video->snippet->defaultAudioLanguage)){
                                    if($video->snippet->defaultAudioLanguage == "en"){
                                        $lang = true;
                                    } else{
                                        $lang = false;
                                    }
                                }

                                if($video->status->embeddable == true && $lang){

                                    
                                        // block channels that frequently mistag their streams
                                        array_push($videos, [
                                            "id" => $id,
                                            "title" => $video->snippet->title,
                                            "description" => $video->snippet->description,
                                            "thumbnail" => $video->snippet->thumbnails->medium->url,
                                            "views" => $video->statistics->viewCount,
                                            "channel" => $video->snippet->channelId,
                                            "type" => 1
                                        ]);
                                    
                                }
                            }

                            }
                        }
                    }
                }
            }

            if (isset($results['info']['nextPageToken'])) {
                $params['pageToken'] = $results['info']['nextPageToken'];
            } else {
                $i = $loopLimit + 1;
            }
        }

        

        if($category == "video" || $category == "gaming"){
            $twitch = new Twitch;
            $i = 0;
            do {
                $nextCursor = null;

                if (isset($search)) {
                    $nextCursor = $search->next();
                }

                $search = $twitch->getStreams(['language' => 'en'], $nextCursor);

                foreach ($search->data() as $twitchVideo){

                    array_push($videos,[
                        "id" => $twitchVideo->user_id,
                        "title" => $twitchVideo->title,
                        "description" => $twitchVideo->game_name,
                        "thumbnail" => parseTwitchThumbnailURL($twitchVideo->thumbnail_url),
                        "views" => $twitchVideo->viewer_count,
                        "channel" => $twitchVideo->user_id,
                        "type" => 2
                    ]);

                    $i++;
                }
                if($i > $loopLimit * 3){
                    break;
                }
            }
            while ($search->hasMoreResults() && $i <= $loopLimit * 3);
        }

        return $videos;
    }
    /*
    public function populate(){
        $categories = ["news", "sports", "music", "gaming", "discussion", "talk"];

        foreach($categories as $category){

        $i = 0;
        $y = 0;

        $paging = true;

        $params = [
            'q' => "live $category", 
            'type' => 'live',
            'part' => 'id, snippet',
            'maxResults'    => 50
        ];

        while($paging){
            $i++;

            $results = Youtube::searchAdvanced($params, true);

            foreach ($results as $result) {
                echo "<pre>";
                var_dump($result);
                echo "</pre>";
                if(is_array($result)){
                    foreach ($result as $key => $video) {
                        if(isset($video->id->videoId)){
                            $id = $video->id->videoId;
                            if($video->snippet->liveBroadcastContent == "live"){
                                $video = Youtube::getVideoInfo($id);
                                $lang = false;

                                if(isset($video->snippet->defaultAudioLanguage)){
                                    if($video->snippet->defaultAudioLanguage == "en"){
                                        $lang = true;
                                    }
                                }

                                if(isset($video->snippet->defaultLanguage)){
                                    if($video->snippet->defaultLanguage == "en"){
                                        $lang = true;
                                    } else{
                                        $lang = false;
                                    }
                                }

                                if($video->status->embeddable == true && $lang){
                                    if(Events::where('iframe', $id)->count() == 0){

                                        Events::create([
                                        'iframe' => $id,
                                        "title" => $video->snippet->title,
                                        "thumbnail" => $video->snippet->thumbnails->medium->url,
                                        "source" => 1,
                                        "views" => $video->statistics->viewCount,
                                        "category" => $category
                                    ]);
                                    $y++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (isset($results['info']['nextPageToken'])) {
                $params['pageToken'] = $results['info']['nextPageToken'];
            } else {
                $paging = false;
                break;
            }
        }

        echo "<br> Added $y new $category videos! <br>";
        }
    }


    public function generate(){
        $twitch = new Twitch;
        $i = 0;
        do {
            $nextCursor = null;

            if (isset($search)) {
                $nextCursor = $search->next();
            }

            $search = $twitch->getStreams(['language' => 'en'], $nextCursor);

            foreach ($search->data() as $twitchVideo){
                Events::updateOrCreate([
                    "iframe" => $twitchVideo->id,
                    "title" => $twitchVideo->title,
                    "description" => $twitchVideo->game_name,
                    "thumbnail" => $this->parseTwitchThumbnailURL($twitchVideo->thumbnail_url),
                    "source" => 2,
                    "channel" => $twitchVideo->user_name,
                    "views" => $twitchVideo->viewer_count,
                    'category' => "gaming"
                ]);
                $i++;
            }
            if($i > 4000){
                break;
            }
        }
        while ($search->hasMoreResults() && $i <= 4000);
    }

    */

    public function getVideoDetails($id, $source){
        if($source == 1){
            // YT
            try{
                $result = Youtube::getVideoInfo($id);

                return [
                    "id" => $result->id,
                    "title" => $result->snippet->title,
                    "description" => $result->snippet->description,
                    "thumbnail" => $result->snippet->thumbnails->medium->url,
                    "source" => 1,
                    "channel" => $result->snippet->channelId,
                    "views" => $result->statistics->viewCount,
                    "page" => NULL
                ];
            }
            catch(Exception $e){
                $result = Events::where('iframe', $id)->first();

                return [
                    "id" => $result->iframe,
                    "title" => $result->title,
                    "description" => NULL,
                    "thumbnail" => $result->thumbnail,
                    "source" => 1,
                    "channel" => $result->channel,
                    "views" => $result->views,
                    "page" => NULL
                ];
            }
        }

        if($source == 2){
            // twitch
            $twitch = new Twitch;

            $twitchVideo = $twitch->getVideos(["id" => $id]);

            dd($twitchVideo);

            $twitchVideo = $twitchVideo->data()[0];

            return [
                "id" => $twitchVideo->id,
                "title" => $twitchVideo->title,
                "description" => $twitchVideo->description,
                "thumbnail" => $this->parseTwitchThumbnailURL($twitchVideo->thumbnail_url),
                "source" => 2,
                "channel" => $twitchVideo->user_name,
                "views" => $twitchVideo->viewer_count,
                "page" => NULL
            ];
        }

        if($source == 3){
            $result = Events::where('iframe', $id)->first();
                return [
                    "id" => $result->iframe,
                    "title" => $result->title,
                    "description" => NULL,
                    "thumbnail" => $result->thumbnail,
                    "source" => 3,
                    "channel" => $result->channel,
                    "views" => $result->views,
                    "page" => NULL
                ];
        }

        return NULL;
    }

    public function blockChannel($channel){
        if(Auth::check()){
            if(Auth::user()->id == 6){
                Blocked::create([
                    "channel" => $channel
                ]);
            }
        }
    }
}
