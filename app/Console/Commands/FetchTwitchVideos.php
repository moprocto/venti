<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use romanzipp\Twitch\Twitch as Twitch;
use App\Models\Event as Events;
use Carbon;
use Session;
use Redirect;
use View;
use Storage;


class FetchTwitchVideos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FetchVideos:Twitch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will retrieve live videos from Twitch';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {      
        Events::where('source', 2)->forceDelete();

        $twitch = new Twitch;
        $i = 0;
        $vids = 0;
        do {
            $nextCursor = null;

            if (isset($search)) {
                $nextCursor = $search->next();
            }

            $search = $twitch->getStreams(['language' => 'en'], $nextCursor);

            foreach ($search->data() as $twitchVideo){
                Events::updateOrCreate(["iframe" => $twitchVideo->id],[
                    "iframe" => $twitchVideo->id,
                    "title" => $twitchVideo->title,
                    "description" => $twitchVideo->game_name,
                    "thumbnail" => parseTwitchThumbnailURL($twitchVideo->thumbnail_url),
                    "source" => 2,
                    "channel" => $twitchVideo->user_name,
                    "views" => $twitchVideo->viewer_count,
                    'category' => "gaming"
                ]);

                $vids++;
                
            }
            $i++;
            if($i > 9){
                break;
            }
        }
        while ($search->hasMoreResults() && $i <= 10);

        echo "\nAdded $vids from Twitch \n\n";
    }
}