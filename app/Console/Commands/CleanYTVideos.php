<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Alaouy\Youtube\Facades\Youtube as Youtube;
use App\Models\Event as Events;
use App\Models\Blocked as Blocked;
use Carbon;
use Session;
use Redirect;
use View;
use Storage;
use Exception;


class CleanYTVideos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CleanVideos:YouTube';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will delete videos that are no longer available on YouTube';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $i = 0;
        $videos = Events::where('source', 1)->oldest()->get();

        foreach($videos as $video){
            // YT
            try{
                $result = Youtube::getVideoInfo($video->iframe);

                if(is_object($result)){
                    if($result->snippet->liveBroadcastContent == "none"){
                        $video->delete();
                        $i++;
                    }
                } else{
                    $video->delete();
                    $i++;
                }
            }
            catch(Exception $e){
                dd($e);
            }
        }

        echo "Deleted $i videos!";
    }
}