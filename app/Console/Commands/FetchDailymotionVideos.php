<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use App\Models\Event as Events;
use \dailymotiton\sdk\Dailymotion as Dailymotion;
use Carbon;
use Session;
use Redirect;
use View;
use Storage;


class FetchDailymotionVideos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FetchVideos:Dailymotion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will retrieve live videos from Dailymsotion';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {      
        try
        {

            Events::where('source', 3)->delete();

            $videos = json_decode(file_get_contents(env("DAILYMOTION_URL")));

            $i = 0;

            foreach($videos->list as $video){
                if($video->allow_embed === true && $video->onair === true ){
                    $channel = $video->channel;
                    Events::updateOrCreate(["iframe" => $video->id],[
                        "iframe" => $video->id,
                        "title" => $video->title,
                        "description" => $video->description,
                        "thumbnail" => $video->thumbnail_720_url,
                        "source" => 3,
                        "channel" => $video->owner,
                        "views" => $video->views_total,
                        'category' => $channel
                    ]);
                    $i++;
                }   
            }

            echo "$i views added";
        }
        catch (Exception $e)
        {
            // If the SDK doesn't have any access token stored in memory, it tries to
            // redirect the user to the Dailymotion authorization page for authentication.
            dd($e);
        }
    }
}