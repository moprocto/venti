<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Alaouy\Youtube\Facades\Youtube as Youtube;
use App\Models\Event as Events;
use App\Models\Blocked as Blocked;
use Carbon;
use Session;
use Redirect;
use View;
use Storage;
use Exception;


class FetchYTVideos extends Command
{
    /**
     * The name and signature of the console command.
     * /home/forge/venti.co/artisan FetchVideos:YouTube
     * @var string
     */
    protected $signature = 'FetchVideos:YouTube';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will retrieve live videos from YouTube';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $categories = ["music", "radio", "gaming", "finance", "economy", "news", "politics"];

        foreach($categories as $category){
            $i = 0;

            $params = [
                'q' => "$category", 
                'type' => 'video',
                'eventType' => 'live',
                'part' => 'id, snippet',
                'maxResults'    => 50
            ];

            try{
                while($i < 6){

                    $results = Youtube::searchAdvanced($params, true);

                    foreach ($results as $result) {
                        if(is_array($result)){
                            foreach ($result as $key => $video) {
                                if(isset($video->id->videoId)){
                                    $id = $video->id->videoId;
                                    if(Blocked::where('channel', $video->snippet->channelId)->count() == 0){
                                        // block channels that frequently mistag their streams
                                        if($video->snippet->liveBroadcastContent == "live"){
                                            try{
                                                $video = Youtube::getVideoInfo($id);
                                            } 
                                            catch(Exception $e){
                                                break;
                                            }
                                            
                                            $lang = false;

                                            if(isset($video->snippet->defaultLanguage)){
                                                if($video->snippet->defaultLanguage == "en"){
                                                    $lang = true;
                                                } else{
                                                    $lang = false;
                                                }
                                            }

                                             if(isset($video->snippet->defaultAudioLanguage)){
                                                if($video->snippet->defaultAudioLanguage == "en"){
                                                    $lang = true;
                                                } else{
                                                    $lang = false;
                                                }
                                            }

                                            if($video->status->embeddable == true && $lang){

                                                // convert cat

                                                if($category == "economy"){
                                                    $category = "finance";
                                                }

                                                if($category == "radio"){
                                                    $category = "music";
                                                }

                                                if($category == "politics"){
                                                    $category == "news";
                                                }

                                                Events::updateOrCreate(["iframe" => $id],[
                                                    'iframe' => $id,
                                                    "title" => $video->snippet->title,
                                                    "description" => $video->snippet->description,
                                                    "thumbnail" => $video->snippet->thumbnails->medium->url,
                                                    "source" => 1,
                                                    "channel" => $video->snippet->channelId,
                                                    "views" => $video->statistics->viewCount,
                                                    "category" => $category
                                                ]); 
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (isset($results['info']['nextPageToken'])) {
                            $params['pageToken'] = $results['info']['nextPageToken'];
                            $i++;
                        } else {
                            $i = 6;
                        }
                    }
                }
            
            } catch(Exception $e) {
                dd($e);
                echo "\nYouTube API Unavailable\n\n";
                return "";
            }
        }
    }
}