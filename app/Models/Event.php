<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'title', 'iframe', 'type', 'thumbnail', 'source', 'views', 'channel', 'category', 'description'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function scopeBlocked(){
        if(Blocked::where('channel', $this->channel)->count()){
            return true;
        }
        return false;
    }
}
